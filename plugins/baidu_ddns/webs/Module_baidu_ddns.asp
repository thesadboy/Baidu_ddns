<title>百度DDNS</title>
<content>
    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/tomato.js"></script>
    <script type="text/javascript" src="/js/advancedtomato.js"></script>
    <style type="text/css">
    .box, #v2ray_tabs {
    min-width:720px;
    }
    </style>
    <script type="text/javascript">
    var dbus;
    get_dbus_data();
    function get_dbus_data() {
        $.ajax({
            type: "GET",
            url: "/_api/baidu_ddns",
            dataType: "json",
            async: false,
            success: function(data) {
                dbus = data.result[0];
            }
        });
    }
    function get_last_log(){
        $.ajax({
           url:'/_api/',
           type: 'POST',
           cache: false,
           dataType:'text',
           data: JSON.stringify({
               id: parseInt(Math.random() * 100000000),
               method: 'baidu_ddns_config.sh',
               params: ['log'],
               fields: ''
           }),
           success: function(data){
               var log = data ? data.replace(/^{"result": "/,'').replace(/"}$/,'') : '';
               $('#log-content').html(log)
           }
       });
    }
    function verifyFields(r){}

    function showMsg(Outtype, title, msg) {
        hideMsg();
        $('#' + Outtype).html('<h5>' + title + '</h5>' + msg + '<a class="close"><i class="icon-cancel"></i></a>');
        $('#' + Outtype).show();
    }
    function hideMsg(){
        $('#msg_success').hide();
        $('#msg_warring').hide();
        $('#msg_error').hide();
    }

    function save(){
        dbus['baidu_ddns_basic_enable'] = $('#_baidu_ddns_basic_enable').get(0).checked ? 1 : 0;
        dbus['baidu_ddns_check'] = $('#_baidu_ddns_check').val();
        dbus['baidu_ddns_access_key'] = $('#_baidu_ddns_access_key').val();
        dbus['baidu_ddns_secret_key'] = $('#_baidu_ddns_secret_key').val();
        dbus['baidu_ddns_domain'] = $('#_baidu_ddns_domain').val();
        showMsg('msg_warring', '正在提交数据！', '<b>正在提交数据，请稍后</b>');
        $.ajax({
            url:'/_api/',
            type: 'POST',
            async: true,
            cache: false,
            dataType:'json',
            data: JSON.stringify({
                id: parseInt(Math.random() * 100000000),
                method: 'baidu_ddns_config.sh',
                params: ['save'],
                fields: dbus
            }),
            success:function(data){
                showMsg('msg_success', '提示', data.result);
                get_last_log();
                setTimeout(hideMsg, 3000);
            }
        });
    }
    </script>
    <div class="box">
        <div class="heading">
           百度DDNS
           <a href="#/soft-center.asp" class="btn" style="float:right;border-radius:3px;margin-right:5px;margin-top:0px;">返回</a>
        </div>
        <div class="content">
        百度DDNS是基于百度云加速域名解析的一个DDNS模块，是为了解决使用百度云加速做域名解析的朋友无法使用DDNS的问题，如何使用百度云加速进行域名解析，请访问<a href="https://su.baidu.com" target="_blank">百度云加速</a>查看<br>
        项目源码地址<a href="https://gitee.com/thesadboy/Baidu_ddns/tree/master" target="_blank">Baidu DDns</a>
        </div>
    </div>
    <div class="box">
        <div class="heading">日志</div>
        <div class="content" id="log-content"></div>
    </div>
    <div class="box">
        <div class="heading"></div>
        <div class="content">
            <fieldset>
                <label class="col-sm-3 control-left-label" for="_baidu_ddns_version">版本</label>
                <div class="col-sm-9">
                    <div id="_baidu_ddns_version" style="height: 29px;line-height: 42px;">
                        <script type="text/javascript">$('#_baidu_ddns_version').html(dbus.baidu_ddns_version)</script>
                    </div>
                </div>
            </fieldset>
            <div id="baidu_ddns_panel"></div>
            <script type="text/javascript">
            $('#baidu_ddns_panel').forms([
                {title: '开关', name:'baidu_ddns_basic_enable', type: 'checkbox', value: dbus.baidu_ddns_basic_enable == 1},
                {title: '检测时间间隔', name:'baidu_ddns_check', type: 'select', options:[['5','5 分钟'],['10','10 分钟'],['15','15 分钟'],['20','10 分钟'],['25','25 分钟'],['30','30 分钟'],['35','35 分钟'],['40','40 分钟'],['45','45 分钟'],['50','50 分钟'],['55','55 分钟']], value: dbus.baidu_ddns_check, suffix:'基于cron定时任务'},
                {title: 'ACCESS_KEY', name: 'baidu_ddns_access_key', type: 'password', value: dbus.baidu_ddns_access_key, suffix: '请在百度云加速》控制台》账户中心》账户概览 中获取'},
                {title: 'ACCESS_KEY', name: 'baidu_ddns_secret_key', type: 'password', value: dbus.baidu_ddns_secret_key, suffix: '请在百度云加速》控制台》账户中心》账户概览 中获取'},
                {title: '二级域名', name: 'baidu_ddns_domain', type: 'text', value: dbus.baidu_ddns_domain, suffix: '<lable>请将域名写全，例如 www.test.com,不要写顶级域名，例如 test.com</lable>'},
            ])
            </script>
        </div>
    </div>
    <div id="msg_warring" class="alert alert-warning icon" style="display:none;"></div>
    <div id="msg_success" class="alert alert-success icon" style="display:none;"></div>
    <div id="msg_error" class="alert alert-error icon" style="display:none;"></div>
    <button type="button" value="Save" id="save-button" onclick="save()" class="btn btn-primary">提交 <i class="icon-check"></i></button>
    <script type="text/javascript">
        get_last_log();
        setInterval(get_last_log, 5000);
    </script>
</content>