#! /bin/sh

export KSROOT=/koolshare
source $KSROOT/scripts/base.sh
eval `dbus export baidu_ddns_`

sleep 1

rm -rf $KSROOT/scripts/baidu_ddns*
rm -rf $KSROOT/init.d/S99baidu_ddns.sh
rm -rf $KSROOT/baidu_ddns
rm -rf $KSROOT/bin/baidu_ddns
rm -rf $KSROOT/webs/Module_baidu_ddns.asp
rm -rf $KSROOT/webs/res/icon-baidu_ddns.png
rm -rf $KSROOT/webs/res/icon-baidu_ddns-bg.png
rm -rf $KSROOT/scripts/uninstall_baidu_ddns.sh

dbus remove softcenter_module_baidu_ddns_install
dbus remove softcenter_module_baidu_ddns_version
dbus remove softcenter_module_baidu_ddns_name
dbus remove softcenter_module_baidu_ddns_title
dbus remove softcenter_module_baidu_ddns_description
dbus remove baidu_ddns_check
dbus remove baidu_ddns_access_key
dbus remove baidu_ddns_secret_key
dbus remove baidu_ddns_domain
