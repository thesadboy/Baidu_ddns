#!/bin/sh
export KSROOT=/koolshare
source $KSROOT/scripts/base.sh
eval $(dbus export baidu_ddns_)
VERSION=1.1

mkdir -p $KSROOT/init.d
mkdir -p $KSROOT/baidu_ddns
mkdir -p /tmp/upload

[ "$baidu_ddns_basic_enable" == "1" ] && $KSROOT/scripts/baidu_ddns_config.sh stop >/dev/null 2>&1

cp -rf /tmp/baidu_ddns/bin/* $KSROOT/bin/
cp -rf /tmp/baidu_ddns/init.d/* $KSROOT/init.d/
cp -rf /tmp/baidu_ddns/baidu_ddns/* $KSROOT/baidu_ddns/
cp -rf /tmp/baidu_ddns/scripts/* $KSROOT/scripts/
cp -rf /tmp/baidu_ddns/webs/* $KSROOT/webs/
cp /tmp/baidu_ddns/uninstall.sh $KSROOT/scripts/uninstall_baidu_ddns.sh

chmod a+x $KSROOT/scripts/baidu_ddns_*
chmod a+x $KSROOT/init.d/*
chmod a+x $KSROOT/bin/baidu_ddns

# add icon into softerware center
dbus set baidu_ddns_version=$VERSION
dbus set softcenter_module_baidu_ddns_install=1
dbus set softcenter_module_baidu_ddns_version=$VERSION
dbus set softcenter_module_baidu_ddns_name="百度 DDNS"
dbus set softcenter_module_baidu_ddns_title="百度 DDNS"
dbus set softcenter_module_baidu_ddns_description="百度云加速解析"

if [ -z "$baidu_ddns_check" ]; then
  dbus set baidu_ddns_check=5
fi

rm -rf $KSROOT/install.sh

# remove old files if exist
find /etc/rc.d/ -name *baidu_ddns.sh* | xargs rm -rf

sleep 1
rm -rf /tmp/baidu_ddns >/dev/null 2>&1
